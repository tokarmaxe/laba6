package com.example.maxim.laba6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        EditText stringX = (EditText) findViewById(R.id.x);
        EditText stringY = (EditText) findViewById(R.id.y);
        if(stringX.getText().toString().length() != 9 || stringY.getText().toString().length() !=9) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Ваше сообщение не состоит из 9ти букв!",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        else {
            attack(stringX.getText().toString().toUpperCase(), stringY.getText().toString().toUpperCase());
        }
    }

    public void attack(String x, String y) {
        char[] masX = x.toCharArray();
        char[] masY = y.toCharArray();

        //make asd
        int[][] matrixX = new int[3][3];
        int[][] matrixY = new int[3][3];
        int count = 0;
        for (int i = 0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                matrixX[i][j]=(int)masX[count]-65;
                matrixY[i][j]=(int)masY[count]-65;
                count++;
            }
        }
        int delta = deltaMatrix(matrixX);

        int[] y1 = new int[3];
        int[] y2 = new int[3];
        int[] y3 = new int[3];

        y1[0]=matrixY[0][0];
        y1[1]=matrixY[1][0];
        y1[2]=matrixY[2][0];

        y2[0]=matrixY[0][1];
        y2[1]=matrixY[1][1];
        y2[2]=matrixY[2][1];

        y3[0]=matrixY[0][2];
        y3[1]=matrixY[1][2];
        y3[2]=matrixY[2][2];

        int[][] newM = new int[3][3];

        for (int i =0; i<3; i++)
        {
            for (int j = 0; j<3; j++)
            {
                newM[i][j]=matrixX[i][j];
            }
        }

        int rev = findTheReverse(delta);

        int[][] h11Matrix = changeCol(newM, y1, 0);
        int deltah11 = deltaMatrix(h11Matrix);
        int h11 = (deltah11*findTheReverse(delta))%26;

        int[][] h12Matrix = changeCol(newM, y1, 1);
        int deltah12 = deltaMatrix(h12Matrix);
        int h12 = (deltah12*findTheReverse(delta))%26;

        int[][] h13Matrix = changeCol(newM, y1, 2);
        int deltah13 = deltaMatrix(h13Matrix);
        int h13 = (deltah11*findTheReverse(delta))%26;

        int[][] h21Matrix = changeCol(newM, y2, 0);
        int deltah21 = deltaMatrix(h21Matrix);
        int h21 = (deltah21*findTheReverse(delta))%26;

        int[][] h22Matrix = changeCol(newM, y2, 1);
        int deltah22 = deltaMatrix(h22Matrix);
        int h22 = (deltah22*findTheReverse(delta))%26;

        int[][] h23Matrix = changeCol(newM, y2, 2);
        int deltah23 = deltaMatrix(h23Matrix);
        int h23 = (deltah23*findTheReverse(delta))%26;

        int[][] h31Matrix = changeCol(newM, y3, 0);
        int deltah31 = deltaMatrix(h31Matrix);
        int h31 = (deltah31*findTheReverse(delta))%26;

        int[][] h32Matrix = changeCol(newM, y3, 1);
        int deltah32 = deltaMatrix(h32Matrix);
        int h32 = (deltah32*findTheReverse(delta))%26;

        int[][] h33Matrix = changeCol(newM, y3, 2);
        int deltah33 = deltaMatrix(h33Matrix);
        int h33 = (deltah33*findTheReverse(delta))%26;

        char[] result = new char[]{(char)(h11+65),(char)(h12+65),(char)(h13+65),(char)(h21+65),(char)(h22+65),(char)(h23+65),(char)(h31+65),(char)(h32+65),(char)(h33+65)};

        String newStr = new String(result);
        display(newStr);
    }

    public int findTheReverse(int num){
        int reverse = 0;
        for(int i = 1; i<31;i++)
        {
            if((num*i)%31==1){
                reverse=i;
                break;
            }
        }
        return reverse;
    }

    public int deltaMatrix(int[][] mas)
    {
        int a = (mas[0][0]*mas[1][1]*mas[2][2] + mas[0][1]*mas[1][2]*mas[2][0] + mas[0][2]*mas[1][0]*mas[2][1] -
                mas[0][2]*mas[1][1]*mas[2][0] - mas[0][1]*mas[1][0]*mas[2][2] - mas[0][0]*mas[1][2]*mas[2][1])%26;
        if(a > 0) {
            return a;
        }else {
            return a+26;
        }
    }

    public int[][] changeCol(int[][] mas, int[] y, int col)
    {
        int[][] m = new int[3][3];
        for (int i =0; i<3; i++)
        {
            for (int j = 0; j<3; j++)
            {
                m[i][j]=mas[i][j];
            }
        }
        for (int i = 0; i<3; i++)
        {
            m[i][col] = y[i];
        }
        return m;
    }
    public void display(String result) {
        TextView ownView = (TextView) findViewById(R.id.result);
        ownView.setText(result);
    }
}